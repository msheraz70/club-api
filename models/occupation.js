const Joi = require('joi');
const mongoose = require('mongoose');
 
const Occupation = mongoose.model('Occupation', new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
}));
 
exports.Occupation = Occupation;