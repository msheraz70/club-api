const Joi = require('joi');
const mongoose = require('mongoose');
 
const Gallery = mongoose.model('Gallery', new mongoose.Schema({
    galleryName: {
        type: String,
        required: true
    },
    galleryDescription: {
        type: String,
        required: false
    },
    galleryImages: {
        type: Array,
        required: false
    }
}));
 
exports.Gallery = Gallery;