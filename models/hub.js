const Joi = require('joi');
const mongoose = require('mongoose');
 
const Hub = mongoose.model('Hub', new mongoose.Schema({
    hubNumber: {
        type: String,
        required: false
    },
    hubName: {
        type: String,
        required: true
    },
    hubAddress: {
        type: String,
        required: false
    },
    hubPhone: {
        type: String,
        required: false
    },
    hubEmail: {
        type: String,
        required: false
    },
    hubAdmin: {
        type: String,
        required: false
    },
}));
 
exports.Hub = Hub;