const Joi = require('joi');
const mongoose = require('mongoose');
 
const User = mongoose.model('User', new mongoose.Schema({
    hubName: {
        type: String,
        required: false,
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 1024
    },
    userRole: {
        type: String,
        required: true,
    },
    identityProof: {
        type: String,
        required: false,
    },
    personalDetails:{
        name: {
            type: String,
            required: true,
            maxlength: 50
        },
        membershipType: {
            type: String,
            required: true
        },
        phoneNumber: {
            type: String,
            required: false
        },
        correspondingAddress: {
            type: String,
            required: false
        },
        permanentAddress: {
            type: String,
            required: false
        },
        profession: {
            type: String,
            required: false
        },
        interests: {
            type: Array,
            required: false
        },
        maritalStatus: {
            type: String,
            required: false
        },
        birthday: {
            type: String,
            required: false
        },
        anniversary: {
            type: String,
            required: false
        },
        bloodGroup: {
            type: String,
            required: false
        },
        facebookId: {
            type: String,
            required: false
        },
        instagramId: {
            type: String,
            required: false
        },
        aboutYou: {
            type: String,
            required: false
        }
    },
    identityProof: {
        type: String,
        required: false
    },
    paymentDetails: {
        paymentDate: {
            type: String,
            required: false
        },
        paymentAmount: {
            type: String,
            required: false
        },
        paymentType: {
            type: String,
            required: false
        },

    }

}));
 
function validateUser(user) {
    const schema = {
        hubName: Joi.string().allow('').allow(null),
        userId: Joi.string().allow('').allow(null),
        email: Joi.string().max(255).required().email(),
        password: Joi.string().min(8).max(255).required(),
        userRole: Joi.string().required(),
        identityProof: Joi.string().allow('').allow(null),
        personalDetails: Joi.object({
            name: Joi.string().min(3).max(50).required(),
            membershipType: Joi.string().required(),
            phoneNumber: Joi.string().allow('').allow(null),
            correspondingAddress: Joi.string().allow('').allow(null),
            permanentAddress: Joi.string().allow('').allow(null),
            profession: Joi.string().allow('').allow(null),
            interests: Joi.array().allow('').allow(null),
            maritalStatus: Joi.string().allow('').allow(null),
            birthday: Joi.string().allow('').allow(null),
            anniversary: Joi.string().allow('').allow(null),
            bloodGroup: Joi.string().allow('').allow(null),
            facebookId: Joi.string().allow('').allow(null),
            instagramId: Joi.string().allow('').allow(null),
            aboutYou: Joi.string().allow('').allow(null)
        }),
        identityProof: Joi.string().allow('').allow(null),
        paymentDetails: Joi.object({
            paymentDate: Joi.string().allow('').allow(null),
            paymentAmount: Joi.string().allow('').allow(null),
            paymentType: Joi.string().allow('').allow(null)
        }),
    };
    return Joi.validate(user, schema);
}
 
exports.User = User;
exports.validate = validateUser;