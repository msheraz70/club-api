const Joi = require('joi');
const mongoose = require('mongoose');
 
const Facility = mongoose.model('Facility', new mongoose.Schema({
    facilityName: {
        type: String,
        required: true
    },
    facilityDescription: {
        type: String,
        required: false
    },
    facilityLocation: {
        type: String,
        required: false
    },
    facilityTimings: {
        type: String,
        required: false
    },
    facilityImages: {
        type: Array,
        required: false
    }
}));
 
exports.Facility = Facility;