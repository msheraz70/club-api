const Joi = require('joi');
const mongoose = require('mongoose');
 
const Message = mongoose.model('Message', new mongoose.Schema({
    from: {
        type: String,
        required: true
    },
    to: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    }
}));
 
exports.Message = Message;