const Joi = require('joi');
const mongoose = require('mongoose');
 
const Email = mongoose.model('Email', new mongoose.Schema({
    from: {
        type: String,
        required: false
    },
    userName: {
        type: String,
        required: false
    },
    password: {
        type: String,
        required: false
    },
    newMemberMailSubject: {
        type: String,
        required: false
    },
    newMemberMailBody: {
        type: String,
        required: false
    },
    forgotPasswordMailSubject: {
        type: String,
        required: false
    },
    forgotPasswordMailBody: {
        type: String,
        required: false
    }
}));
 
exports.Email = Email;