const Joi = require('joi');
const mongoose = require('mongoose');
 
const Event = mongoose.model('Event', new mongoose.Schema({
    eventName: {
        type: String,
        required: true
    },
    eventDetails: {
        eventDate: {
            type: String,
            required: true
        },
        eventTime: {
            type: String,
            required: true
        },
        eventCategory: {
            type: String,
            required: false
        },
    },
    eventOrganiserDetails: {
        eventOrganiserName: {
            type: String,
            required: true
        },
        eventOrganiserPhone: {
            type: String,
            required: false
        },
        eventOrganiserEmail: {
            type: String,
            required: false
        }
    },
    eventLocationDetails: {
        eventAddress: {
            type: String,
            required: true
        },
        eventMapURL: {
            type: String,
            required: false
        }
    },
    eventLikes: {
        type: Number,
        required: false
    },
    eventDislikes: {
        type: Number,
        required: false
    },
    paths: {
        type: Array,
        required: false
    }
}));

function validateEvent(event) {
    const schema = {
        userId: Joi.string().allow('').allow(null),
        eventName: Joi.string().required(),
        eventDetails: Joi.object({
            eventDate: Joi.string().required(),
            eventTime: Joi.string().required(),
            eventCategory: Joi.string().allow('').allow(null),
        }),
        eventOrganiserDetails: Joi.object({
            eventOrganiserName: Joi.string().required(),
            eventOrganiserPhone: Joi.string().allow('').allow(null),
            eventOrganiserEmail: Joi.string().allow('').allow(null)
        }),
        eventLocationDetails: Joi.object({
            eventAddress: Joi.string().required(),
            eventMapURL: Joi.string().allow('').allow(null)
        }),
        eventLikes: Joi.number().positive().allow(0),
        eventDislikes: Joi.number().positive().allow(0),
        paths: Joi.array().allow('').allow(null)
    };
    return Joi.validate(event, schema);
}
 
exports.Event = Event;
exports.validate = validateEvent;