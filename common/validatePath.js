var fs = require('fs');

module.exports = function(req, res, next) {
    uploadPath = 'public/images/uploads/'+req.folderName+req.name;
    fs.exists(uploadPath, function(exists) {
       if(exists) {
         next();
       }
       else {
         fs.mkdir(uploadPath, function(err) {
           if(err) {
             console.log('Error in folder creation');
             next(); 
           }  
           next();
         })
       }
    })
}