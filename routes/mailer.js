const bcrypt = require('bcrypt');
const _ = require('lodash');
const express = require('express');
const router = express.Router();
const nodemailer = require("nodemailer");
var ejs = require('ejs');

 
router.post('/', async (req, res) => {
    sendMail(req,res);
});

sendMail = async function send(req,res) {
    var from = req.body.mailDetails.from;
    var to = req.body.to;
    var subject = req.body.mailDetails.subject;
    var message = req.body.mailDetails.message;
    var htmlMessage = req.body.mailDetails.htmlMessage;
    var userDetails = req.body.userData;
    var smtpTransport = nodemailer.createTransport({
        service: "Gmail",
        auth: {
            user: req.body.mailDetails.userName,
            pass: req.body.mailDetails.password
        }
    });
  
    // send mail with defined transport object
    let info = await smtpTransport.sendMail({
      from: from, // sender address
      to: to, // list of receivers
      subject: ejs.render(subject,userDetails), // Subject line
      text: ejs.render(message,userDetails), // plain text body
      html: ejs.render(htmlMessage,userDetails) // html body
    }).then(res => { 
        //res.status(200).send({"status":"success","statusCode":"200","data":res});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while sending mail","status":"failure","statusCode":"400"
        });
    });
}
module.exports = router;
module.exports.mail = sendMail;