const bcrypt = require('bcrypt');
const _ = require('lodash');
const { Hub } = require('../models/hub');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    Hub.find()
    .then(hubs => {
        res.send({"status":"success","statusCode":"200","data":hubs});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while retrieving hub list.","status":"failure","statusCode":"400"
        });
    });
});

router.get('/:hubId', async (req, res) => {
    Hub.findById(req.params.hubId)
    .then(hub => {
        if(!hub) {
            return res.status(404).send({
                "message": "Hub not found with id " + req.params.hubId,"status":"failure","statusCode":"400"
            });            
        }
        res.send({"status":"success","statusCode":"200","data":hub});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Hub not found with id " + req.params.hubId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error retrieving Hub with id " + req.params.hubId,"status":"failure","statusCode":"400"
        });
    });
});
 
router.post('/', async (req, res) => {
    // Check if this user already exisits
    let hub = await Hub.findOne({ hubName: req.body.hubName });
    if (hub) {
        return res.status(400).send({"message":"Hub already exists!","status":"failure","statusCode":"404"});
    } else {
        // Insert the new user if they do not exist yet
        hub = new Hub(_.pick(req.body, ['hubNumber', 'hubName', 'hubAddress', 'hubPhone', 'hubEmail', 'hubAdmin']));
        await hub.save();
        res.send({"status":"success","statusCode":"200","data":_.pick(hub, ['_id', 'hubNumber', 'hubName', 'hubAddress', 'hubPhone', 'hubEmail', 'hubAdmin'])});
    }
});

router.put('/', async (req, res) => {
    Hub.findByIdAndUpdate(req.body.hubId, req.body, {new: true})
    .then(hub => {
        if(!hub) {
            return res.status(404).send({
                "message": "Hub not found with ID: " + req.body.hubId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":hub});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
            "message": "Hub not found with id " + req.body.hubId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating Hub with id " + req.body.hubId,"status":"failure","statusCode":"400"
        });
    });
});

router.delete('/:hubId', async (req, res) => {
    Hub.findByIdAndRemove(req.params.hubId)
    .then(hub => {
        if(!hub) {
            return res.status(404).send({
                "message": "Hub not found with id " + req.params.hubId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":hub});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Hub not found with id " + req.params.hubId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error deleting Hub with id " + req.params.hubId,"status":"failure","statusCode":"400"
        });
    });
});

module.exports = router;