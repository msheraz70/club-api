const bcrypt = require('bcrypt');
const _ = require('lodash');
const { Occupation } = require('../models/occupation');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    Occupation.find()
    .then(occupations => {
        res.send({"status":"success","statusCode":"200","data":occupations});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while retrieving occupation list.","status":"failure","statusCode":"400"
        });
    });
});

router.get('/:occupationId', async (req, res) => {
    Occupation.findById(req.params.occupationId)
    .then(occupation => {
        if(!occupation) {
            return res.status(404).send({
                "message": "Occupation not found with id " + req.params.occupationId,"status":"failure","statusCode":"400"
            });            
        }
        res.send({"status":"success","statusCode":"200","data":occupation});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Occupation not found with id " + req.params.occupationId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error retrieving Occupation with id " + req.params.occupationId,"status":"failure","statusCode":"400"
        });
    });
});
 
router.post('/', async (req, res) => {
    // Check if this user already exisits
    let occupation = await Occupation.findOne({ name: req.body.name });
    if (occupation) {
        return res.status(400).send({"message":"Occupation already exists!","status":"failure","statusCode":"404"});
    } else {
        // Insert the new user if they do not exist yet
        occupation = new Occupation(_.pick(req.body, ['name']));
        await occupation.save();
        res.send({"status":"success","statusCode":"200","data":_.pick(occupation, ['_id', 'name'])});
    }
});

router.put('/', async (req, res) => {
    Occupation.findByIdAndUpdate(req.body.occupationId, {
        name: req.body.name
    }, {new: true})
    .then(occupation => {
        if(!occupation) {
            return res.status(404).send({
                "message": "Occupation not found with ID: " + req.body.occupationId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":occupation});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
            "message": "Occupation not found with id " + req.body.occupationId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating Occupation with id " + req.body.occupationId,"status":"failure","statusCode":"400"
        });
    });
});

router.delete('/:occupationId', async (req, res) => {
    Occupation.findByIdAndRemove(req.params.occupationId)
    .then(occupation => {
        if(!occupation) {
            return res.status(404).send({
                "message": "Occupation not found with id " + req.params.occupationId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":occupation});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Occupation not found with id " + req.params.occupationId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error deleting Occupation with id " + req.params.occupationId,"status":"failure","statusCode":"400"
        });
    });
});

module.exports = router;