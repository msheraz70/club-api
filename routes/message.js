const bcrypt = require('bcrypt');
const _ = require('lodash');
const { Message } = require('../models/message');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    Message.find()
    .then(messages => {
        res.send({"status":"success","statusCode":"200","data":messages});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while retrieving message list.","status":"failure","statusCode":"400"
        });
    });
});

router.get('/:messageId', async (req, res) => {
    Message.findById(req.params.messageId)
    .then(message => {
        if(!message) {
            return res.status(404).send({
                "message": "Message not found with id " + req.params.messageId,"status":"failure","statusCode":"400"
            });            
        }
        res.send({"status":"success","statusCode":"200","data":message});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Message not found with id " + req.params.messageId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error retrieving Message with id " + req.params.messageId,"status":"failure","statusCode":"400"
        });
    });
});
 
router.post('/', async (req, res) => {
        message = new Message(_.pick(req.body, ['from', 'to', 'date', 'title', 'message']));
        await message.save();
        res.send({"status":"success","statusCode":"200","data":_.pick(message, ['_id', 'from', 'to', 'date', 'title', 'message'])});
});

router.put('/', async (req, res) => {
    Message.findByIdAndUpdate(req.body.messageId, req.body, {new: true})
    .then(message => {
        if(!message) {
            return res.status(404).send({
                "message": "Message not found with ID: " + req.body.messageId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":message});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
            "message": "Message not found with id " + req.body.messageId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating Message with id " + req.body.messageId,"status":"failure","statusCode":"400"
        });
    });
});

router.delete('/:messageId', async (req, res) => {
    Message.findByIdAndRemove(req.params.messageId)
    .then(message => {
        if(!message) {
            return res.status(404).send({
                "message": "Message not found with id " + req.params.messageId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":message});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Message not found with id " + req.params.messageId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error deleting Message with id " + req.params.messageId,"status":"failure","statusCode":"400"
        });
    });
});

module.exports = router;