const bcrypt = require('bcrypt');
const _ = require('lodash');
const { Event, validate } = require('../models/event');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    var limit = 0 , skip = 0 , q = {} ;
    if(req.query.limit !== undefined) {
        limit = parseInt(req.query.limit);
    }
    if(req.query.skip !== undefined) {
        skip = parseInt(req.query.skip);
    }
    if(req.query.q !== undefined) {
        q = JSON.parse(req.query.q);
    }
    Event.find(q).limit(limit).skip(skip)
    .then(events => {
        res.send({"status":"success","statusCode":"200","data":events});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while retrieving event list.","status":"failure","statusCode":"400"
        });
    });
});

router.get('/:eventId', async (req, res) => {
    Event.findById(req.params.eventId)
    .then(event => {
        if(!event) {
            return res.status(404).send({
                "message": "Event not found with id " + req.params.eventId,"status":"failure","statusCode":"400"
            });            
        }
        res.send({"status":"success","statusCode":"200","data":event});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Event not found with id " + req.params.eventId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error retrieving Event with id " + req.params.eventId,"status":"failure","statusCode":"400"
        });
    });
});
 
router.post('/', async (req, res) => {

    const { error } = validate(req.body);
    if (error) {
        return res.status(400).send({"message":error.details[0].message,"status":"failure","statusCode":"400"});
    }
    // Check if this user already exisits
    let event = await Event.findOne({ eventName: req.body.eventName });
    if (event) {
        return res.status(400).send({"message":"Event already exists!","status":"failure","statusCode":"404"});
    } else {
        // Insert the new user if they do not exist yet
        event = new Event(_.pick(req.body, ['eventName', 'eventDetails', 'eventOrganiserDetails', 'eventLocationDetails', 'eventLikes', 'eventDislikes', 'paths']));
        await event.save();
        res.send({"status":"success","statusCode":"200","data":_.pick(event, ['_id', 'eventName', 'eventDetails', 'eventOrganiserDetails', 'eventLocationDetails', 'eventLikes', 'eventDislikes', 'paths'])});
    }
});

router.put('/', async (req, res) => {
    Event.findByIdAndUpdate(req.body.eventId, req.body, {new: true})
    .then(event => {
        if(!event) {
            return res.status(404).send({
                "message": "Event not found with ID: " + req.body.eventId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":event});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
            "message": "Event not found with id " + req.body.eventId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating Event with id " + req.body.eventId,"status":"failure","statusCode":"400"
        });
    });
});

router.delete('/:eventId', async (req, res) => {
    Event.findByIdAndRemove(req.params.eventId)
    .then(event => {
        if(!event) {
            return res.status(404).send({
                "message": "Event not found with id " + req.body.eventId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":event});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Event not found with id " + req.body.eventId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error deleting Event with id " + req.body.eventId,"status":"failure","statusCode":"400"
        });
    });
});

module.exports = router;