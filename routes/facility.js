const bcrypt = require('bcrypt');
const _ = require('lodash');
const { Facility } = require('../models/facility');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    var limit = 0 , skip = 0 , q = {} ;
    if(req.query.limit !== undefined) {
        limit = parseInt(req.query.limit);
    }
    if(req.query.skip !== undefined) {
        skip = parseInt(req.query.skip);
    }
    if(req.query.q !== undefined) {
        q = JSON.parse(req.query.q);
    }
    Facility.find(q).limit(limit).skip(skip)
    .then(facilities => {
        res.send({"status":"success","statusCode":"200","data":facilities});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while retrieving facility list.","status":"failure","statusCode":"400"
        });
    });
});

router.get('/:facilityId', async (req, res) => {
    Facility.findById(req.params.facilityId)
    .then(facility => {
        if(!facility) {
            return res.status(404).send({
                "message": "Facility not found with id " + req.params.facilityId,"status":"failure","statusCode":"400"
            });            
        }
        res.send({"status":"success","statusCode":"200","data":facility});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Facility not found with id " + req.params.facilityId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error retrieving Facility with id " + req.params.facilityId,"status":"failure","statusCode":"400"
        });
    });
});
 
router.post('/', async (req, res) => {
    // Check if this user already exisits
    let facility = await Facility.findOne({ facilityName: req.body.facilityName });
    if (facility) {
        return res.status(400).send({"message":"Facility already exists!","status":"failure","statusCode":"404"});
    } else {
        // Insert the new user if they do not exist yet
        facility = new Facility(_.pick(req.body, ['facilityName', 'facilityDescription', 'facilityLocation', 'facilityTimings', 'facilityImages']));
        await facility.save();
        res.send({"status":"success","statusCode":"200","data":_.pick(facility, ['_id', 'facilityName', 'facilityDescription', 'facilityLocation', 'facilityTimings', 'facilityImages'])});
    }
});

router.put('/', async (req, res) => {
    Facility.findByIdAndUpdate(req.body.facilityId, req.body, {new: true})
    .then(facility => {
        if(!facility) {
            return res.status(404).send({
                "message": "Facility not found with ID: " + req.body.facilityId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":facility});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
            "message": "Facility not found with id " + req.body.facilityId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating Facility with id " + req.body.facilityId,"status":"failure","statusCode":"400"
        });
    });
});

router.delete('/:facilityId', async (req, res) => {
    Facility.findByIdAndRemove(req.params.facilityId)
    .then(facility => {
        if(!facility) {
            return res.status(404).send({
                "message": "Facility not found with id " + req.params.facilityId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":facility});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Facility not found with id " + req.params.facilityId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error deleting Facility with id " + req.params.facilityId,"status":"failure","statusCode":"400"
        });
    });
});

module.exports = router;