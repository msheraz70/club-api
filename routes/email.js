const bcrypt = require('bcrypt');
const _ = require('lodash');
const { Email } = require('../models/email');
const express = require('express');
const router = express.Router();
const validateAuth = require('../common/validateAuth');

router.get('/', async (req, res) => {
    Email.find()
    .then(emails => {
        res.send({"status":"success","statusCode":"200","data":emails});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while retrieving emails list.","status":"failure","statusCode":"400"
        });
    });
});

router.get('/:emailId',validateAuth, async (req, res) => {
    Email.findById(req.params.emailId)
    .then(email => {
        if(!email) {
            return res.status(404).send({
                "message": "Email not found with id " + req.params.emailId,"status":"failure","statusCode":"400"
            });            
        }
        res.send({"status":"success","statusCode":"200","data":email});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Email not found with id " + req.params.emailId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error retrieving Email with id " + req.params.emailId,"status":"failure","statusCode":"400"
        });
    });
});
 
router.post('/',validateAuth, async (req, res) => {

        email = new Email(_.pick(req.body, ['from', 'userName', 'password', 'newMemberMailSubject', 'newMemberMailBody', 'forgotPasswordMailSubject', 'forgotPasswordMailBody']));
        await email.save();
        res.send({"status":"success","statusCode":"200","data":_.pick(email, ['_id', 'from', 'userName', 'password', 'newMemberMailSubject', 'newMemberMailBody', 'forgotPasswordMailSubject', 'forgotPasswordMailBody'])});

});

router.put('/',validateAuth, async (req, res) => {
    Email.findByIdAndUpdate(req.body.emailId, req.body, {new: true})
    .then(email => {
        if(!email) {
            return res.status(404).send({
                "message": "Email not found with ID: " + req.body.emailId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":email});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
            "message": "Email not found with id " + req.body.emailId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating Email with id " + req.body.emailId,"status":"failure","statusCode":"400"
        });
    });
});


module.exports = router;