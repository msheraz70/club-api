const bcrypt = require('bcrypt');
const _ = require('lodash');
const { User, validate } = require('../models/user');
const validateAuth = require('../common/validateAuth');
const express = require('express');
const router = express.Router();

router.get('/',validateAuth, async (req, res) => {
    User.find()
    .then(users => {
        res.send({"status":"success","statusCode":"200","data":users});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while retrieving users list.","status":"failure","statusCode":"400"
        });
    });
});

router.get('/:id', validateAuth, async (req, res) => {
    User.findById(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                "message": "User not found with id " + req.params.id,"status":"failure","statusCode":"400"
            });            
        }
        res.send({"status":"success","statusCode":"200","data":user});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "User not found with id " + req.params.id,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error retrieving User with id " + req.params.id,"status":"failure","statusCode":"400"
        });
    });
});

router.post('/', validateAuth, async (req, res) => {
    // First Validate The Request
    const { error } = validate(req.body);
    if (error) {
        return res.status(400).send({"message":error.details[0].message,"status":"failure","statusCode":"400"});
    }
 
    // Check if this user already exisits
    let user = await User.findOne({ email: req.body.email });
    if (user) {
        return res.status(400).send({"message":"User already exists!","status":"failure","statusCode":"404"});
    } else {
        // Insert the new user if they do not exist yet
        user = new User(_.pick(req.body, ['email', 'password', 'userRole', 'hubName', 'identityProof', 'personalDetails', 'paymentDetails']));
        //user = new User(req.body);
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
        await user.save();
        res.send({"status":"success","statusCode":"200","data":_.pick(user, ['_id', 'email', 'password', 'userRole', 'hubName', 'identityProof', 'personalDetails', 'paymentDetails'])});
        //res.send(user);
    }
});

router.put('/',validateAuth, async (req, res) => {
    if(req.body.password) {
        const salt = await bcrypt.genSalt(10);
        req.body.password = await bcrypt.hash(req.body.password, salt);
    }
    if(req.body.emailChanged===true) {
        let user = await User.findOne({ email: req.body.email });
        if (user) {
            return res.status(400).send({"message":"User already exists!","status":"failure","statusCode":"404"});
        }
    }
    User.findByIdAndUpdate(req.body.id,req.body, {new: true})
    .then(user => {
        if(!user) {
            return res.status(404).send({
                "message": "User not found with id " + req.body.id,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":user});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "User not found with id " + req.params.id,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating user with id " + req.params.id,"status":"failure","statusCode":"400"
        });
    });
});

router.delete('/:id',validateAuth, async (req, res) => {
    User.findByIdAndRemove(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                "message": "User not found with id " + req.body.id,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":user});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "User not found with id " + req.body.id,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error deleting user with id " + req.body.id,"status":"failure","statusCode":"400"
        });
    });
});

module.exports = router;