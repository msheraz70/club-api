const bcrypt = require('bcrypt');
const _ = require('lodash');
const { User } = require('../models/user');
const express = require('express');
const router = express.Router();
const http = require('http');
const Mailer = require('./mailer');
function generatePassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}
router.post('/', async (req, res) => {
    let user = await User.findOne({ email: req.body.to });
    if (user) {
    const newPassword = generatePassword();
    req.body.userData.fullName = user.personalDetails.name;
    req.body.userData.password = newPassword;
    const salt = await bcrypt.genSalt(10);
    var body = {
     "password" : await bcrypt.hash(newPassword, salt)
    }
    User.findByIdAndUpdate(user._id,body, {new: true}) 
    .then(user => {
        if(!user) {
            return res.status(404).send({
                "message": "User not found with id " + user._id,"status":"failure","statusCode":"400"
            });
        }
        //Send Mail
        Mailer.mail(req,res); 
        res.send({"status":"success","statusCode":"200","data":[]});
    }).catch(err => {
        if(err.kind === 'ObjectId') { 
            return res.status(404).send({
                "message": "User not found with id " + user._id,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating user with id " + user._id,"status":"failure","statusCode":"400"
        });
    });
    } else {
        return res.status(400).send({"message":"User does not exist!","status":"failure","statusCode":"404"});
    }
});

module.exports = router;
