const bcrypt = require('bcrypt');
const _ = require('lodash');
const { Gallery } = require('../models/gallery');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    var limit = 0 , skip = 0 , q = {} ;
    if(req.query.limit !== undefined) {
        limit = parseInt(req.query.limit);
    }
    if(req.query.skip !== undefined) {
        skip = parseInt(req.query.skip);
    }
    if(req.query.q !== undefined) {
        q = JSON.parse(req.query.q);
    }
    Gallery.find(q).limit(limit).skip(skip)
    .then(galleries => {
        res.send({"status":"success","statusCode":"200","data":galleries});
    }).catch(err => {
        res.status(500).send({
            "message": err.message || "Some error occurred while retrieving gallery list.","status":"failure","statusCode":"400"
        });
    });
});

router.get('/:galleryId', async (req, res) => {
    Gallery.findById(req.params.galleryId)
    .then(gallery => {
        if(!gallery) {
            return res.status(404).send({
                "message": "Gallery not found with id " + req.params.galleryId,"status":"failure","statusCode":"400"
            });            
        }
        res.send({"status":"success","statusCode":"200","data":gallery});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Gallery not found with id " + req.params.galleryId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error retrieving Gallery with id " + req.params.galleryId,"status":"failure","statusCode":"400"
        });
    });
});
 
router.post('/', async (req, res) => {
    // Check if this user already exisits
    let gallery = await Gallery.findOne({ galleryName: req.body.galleryName });
    if (gallery) {
        return res.status(400).send({"message":"Gallery already exists!","status":"failure","statusCode":"404"});
    } else {
        // Insert the new user if they do not exist yet
        gallery = new Gallery(_.pick(req.body, ['galleryName', 'galleryDescription', 'galleryImages']));
        await gallery.save();
        res.send({"status":"success","statusCode":"200","data":_.pick(gallery, ['_id', 'galleryName', 'galleryDescription', 'galleryImages'])});
    }
});

router.put('/', async (req, res) => {
    Gallery.findByIdAndUpdate(req.body.galleryId, req.body, {new: true})
    .then(gallery => {
        if(!gallery) {
            return res.status(404).send({
                "message": "Gallery not found with ID: " + req.body.galleryId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":gallery});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
            "message": "Gallery not found with id " + req.body.galleryId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error updating Gallery with id " + req.body.galleryId,"status":"failure","statusCode":"400"
        });
    });
});

router.delete('/:galleryId', async (req, res) => {
    Gallery.findByIdAndRemove(req.params.galleryId)
    .then(gallery => {
        if(!gallery) {
            return res.status(404).send({
                "message": "Gallery not found with id " + req.params.galleryId,"status":"failure","statusCode":"400"
            });
        }
        res.send({"status":"success","statusCode":"200","data":gallery});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                "message": "Gallery not found with id " + req.params.galleryId,"status":"failure","statusCode":"400"
            });                
        }
        return res.status(500).send({
            "message": "Error deleting Gallery with id " + req.params.galleryId,"status":"failure","statusCode":"400"
        });
    });
});

module.exports = router;