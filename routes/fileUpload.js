const _ = require('lodash');
const express = require('express');
const router = express.Router();
var multer = require('multer');
var fs = require('fs');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        var newDestination = 'public/images/uploads/' + req.body.folderName + '/' + req.body.name;
        cb(null, checkUploadPath(newDestination))
    },
    filename: (req, file, cb) => {
      cb(null, fileName(file))
    }
});

function checkUploadPath(newDestination) {
    if (!fs.existsSync(newDestination)) {
        fs.mkdirSync(newDestination , {recursive: true});
    }
    global.dest = newDestination;
    return newDestination;
}

function fileName(file) {
   var fileName = Date.now() + '-' + file.originalname;
   global.fileList.push(global.dest + '/' + fileName);
   return fileName;
}

function newRequest(req, res, next) {
    global.fileList = [];
    next();
}

var upload = multer({storage: storage});
//var cpUpload = upload.fields([{ name: 'folderName', maxCount: 1 }, { name: 'files', maxCount: 8 }])
router.post('/',newRequest,upload.array('files[]',12), (req, res) => {
    res.send({status:"success","path":global.fileList})
});

router.delete('/', async (req, res) => {
    var path = req.body.imagePath;
    if(req.body.deleteFolder) {
        if( fs.existsSync(path) ) {
            fs.readdirSync(path).forEach(function(file,index){
              var curPath = path + "/" + file;
                fs.unlinkSync(curPath);
            });
            fs.rmdirSync(path);
            res.send({status:"success",message:"Delete Success"});
          }
    } else {
        fs.unlink(req.body.imagePath,function(err){
            if(err) return res.send({status:"failure",message:"Delete Failed"});
            res.send({status:"success",message:"Delete Success"});
       });
    } 
});
module.exports = router;