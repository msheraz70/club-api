const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');
const auth = require('./routes/auth');
const users = require('./routes/users');
const occupation = require('./routes/occupation');
const event = require('./routes/event');
const facility = require('./routes/facility');
const hub = require('./routes/hub');
const gallery = require('./routes/gallery');
const message = require('./routes/message');
const email = require('./routes/email');
const mailer = require('./routes/mailer');
const fileUpload = require('./routes/fileUpload');
const forgotPassword = require('./routes/forgotPassword');
const express = require('express');
const app = express();
const config = require('config');
const validateAuth = require('./common/validateAuth');
var bodyParser = require('body-parser');
var path = require('path');
var cors = require('cors')

if (!config.get('PrivateKey')) {
    console.error('FATAL ERROR: PrivateKey is not defined.');
    process.exit(1);
}

mongoose.connect('mongodb://localhost/mongo-games', { useNewUrlParser: true })
    .then(() => console.log('Now connected to MongoDB!'))
    .catch(err => console.error('Something went wrong', err));
    
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors());

app.use('/public', express.static('public'));

app.use('/api/auth', auth);

app.use('/api/users', users);

app.use('/api/occupation', validateAuth, occupation);

app.use('/api/event', validateAuth, event);

app.use('/api/facility', validateAuth, facility);

app.use('/api/hub', validateAuth, hub);

app.use('/api/gallery', validateAuth, gallery);

app.use('/api/message', validateAuth, message);

app.use('/api/email', email);

app.use('/api/sendmail', validateAuth, mailer);

app.use('/api/uploadFiles', validateAuth, fileUpload);

app.use('/api/forgotPassword', forgotPassword);



const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening on port ${port}...`));